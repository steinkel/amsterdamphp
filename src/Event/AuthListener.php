<?php
namespace App\Event;

use Cake\Log\Log;
use Cake\Event\EventListenerInterface;

class AuthListener implements EventListenerInterface
{
    public function implementedEvents() {
        return array(
            'Auth.loginFailed' => 'loginFailedLog',
        );
    }

    public function loginFailedLog($event, $ip) {
         Log::info('LoginFailed: ' . $ip, 'suspicious');
    }
}