<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 */
class UsersTable extends Table
{

    const ROLE_USER = 100;
    const ROLE_ADMIN = 200;
    const ROLE_SUPERADMIN = 300;
    public $roles = [
        self::ROLE_USER => 'User',
        self::ROLE_ADMIN => 'Admin',
        self::ROLE_SUPERADMIN => 'Super'
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('full_name');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Parties', [
            'foreignKey' => 'party_id'
        ]);
        $this->hasMany('Answers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->allowEmpty('first_name')
            ->allowEmpty('last_name')
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', 'anotherPasswordValidation', [
                'rule' => function ($value, $context) {
                    //do complex validation on password
                    return true;
                }
                ])
            ->add('password', 'strongerPasswordForAdmins', [
                'rule' => ['minLength', 8],
                'message' => 'Admins should have passwords at least 8 chars',
                'on' => function ($context) {
                    return (int)$context['data']['role'] === self::ROLE_ADMIN;
                }
                ])
            ->add('role', 'valid', ['rule' => 'numeric'])
            ->add('role', 'inList', [
                'rule' => ['inList', array_keys($this->roles)],
                'message' => 'Please enter a valid role'
            ])
            ->requirePresence('role', 'create')
            ->notEmpty('role');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['party_id'], 'Parties'));
        return $rules;
    }

}
